db.fruits.aggregate([

    {$match: {$and:[{price:{$lt:50}},{supplier:"Yellow Farms"}]}},
    {$count: "totalNumberOfItemsSmallerThan50"}

])
    
db.fruits.aggregate([

    {$match: {price:{$lt: 30}}},
    {$count: "totalNumberOfItemsLesserthan30"}

])


 db.fruits.aggregate([
    
    {$match:{supplier: "Yellow Farms"}},
    {$group:{_id:"theAveragePriceOfYellowFarms", avgPrice:{$avg:"$price"}}}
    
])

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id:"highestPriceOfRedFarmInc", maxPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id:"lowestPriceOfRedFarmInc", minPrice: {$min: "$price"}}}

])




