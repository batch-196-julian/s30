// db.fruits.insertMany([
// 
//     {
//         name:"Apple",
//         supplier: "Red Farms Inc.",
//         stocks: 20,
//         price: 40,
//         onSale: true
//     },
//     {
//         name:"Banana",
//         supplier: "Yellow Farms",
//         stocks: 15,
//         price: 20,
//         onSale: true
//     },
//     {
//         name:"Kiwi",
//         supplier: "Green farming an Canning",
//         stocks: 25,
//         price: 50,
//         onSale: true
//     },
//     {
//         name:"Mango",
//         supplier: "Yellow Farms",
//         stocks: 10,
//         price: 60,
//         onSale: true
//     },
//     {
//         name:"Dragon Fruit",
//         supplier: "Red Farms Inc.",
//         stocks: 10,
//         price: 60,
//         onSale: true
//     },
//     
//  ])
// 
//  db.fruits.aggregate([
//  
//  {$match:{onSale:true}},
//  {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
//  
//  ])
//  
//  db.fruits.aggregate([
//  
//     {$match: {supplier: "Red Farms Inc."}},
//     {$group: {_id:"RedFarmsInc", totalStocks:{$sum:"$stocks"}}}
//  ])
//   
//  db.fruits.aggregate([
//  
//     {$match: {onSale:true, supplier: "Yellow Farms"}},
//     {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
//  ])
//  
//  db.fruits.aggregate([
//     
//         {$match:{onSale:true}},
//         {$group:{_id:"$supplier", avgStock:{$avg:"$stocks"}}}
//     
//     ])

// db.fruits.aggregate([
// 
//     {$match: {onSale:true}},
//     {$group: {_id:null, avgPrice: {$avg: "$price"}}}
// 
// ])
// db.fruits.aggregate([
// 
//     {$match: {onSale:true}},
//     {$group: {_id:"higheststockOnSale", maxStock: {$max: "$stocks"}}}
// 
// ])
// 
// db.fruits.aggregate([
// 
//     {$match: {onSale:true}},
//     {$group: {_id:null, maxPrice: {$max: "$price"}}}
// 
// ])
    
// db.fruits.aggregate([
// 
//     {$match: {onSale:true}},
//     {$group: {_id:"lowestStocksOnSale", minStock: {$min: "$stocks"}}}
// 
// ])
//     
// db.fruits.aggregate([
// 
//     {$match: {onSale:true}},
//     {$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}
// 
// ])
//     
// db.fruits.aggregate([
// 
//     {$match: {price:{$lte:50}}},
//     {$group: {_id:"lowestStocksOnSale", minStock: {$min:"$stocks"}}}
// 
// ])

// db.fruits.aggregate([
// 
// {$match: {onSale:true}},
// {$count: "itemsOnSale"}
// 
// ])
// 
// db.fruits.aggregate([
// 
// {$match: {price:{$lt:50}}},
// {$count: "itemsPriceLessThan50"}
// 
// ])
// db.fruits.aggregate([
// 
// {$match: {stocks:{$lt:20}}},
// {$count: "forRestock"}
// 
// ])

db.fruits.aggregate([

{$match: {onSale:true}},
{$group: {_id:"$supplier" , totalStocks:{$avg:"$stocks"}}},
{$out: "stocksPerSupplier"}
])


 